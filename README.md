### Script de despligue y montaje

#### Requisitos:

- xfs
- lvm2 (grupo 'dockervg')
- git
- docker


#### Script

Guarde el codigo en un fichero y ejecute el programa.

```
#!/bin/bash

dockerDir="/srv"


read -p "Nombre del sitio: " siteName
read -p "Almacenamiento (G): " siteVol
read -p "Puerto: " puerto


# Creacion de volumen logico

siteVolName=${siteName// /_}

lvcreate -L ${siteVol}G -n "$siteVolName" dockervg

mkfs.xfs /dev/dockervg/$siteVolName

mkdir -p /srv/$siteVolName

# Punto de monjate 

echo "[Unit]
Description=Docker nginx

[Mount]
What=/dev/dockervg/$siteVolName
Where=/srv/$siteVolName
Type=xfs

[Install]
WantedBy=multi-user.target" > /lib/systemd/system/srv-${siteVolName}.mount

systemctl daemon-reload

systemctl enable --now srv-${siteVolName}.mount


# Despliegue de la aplicacion

git clone https://gitlab.com/sergio.pernas2/bootstrap.git /srv/$siteVolName


sed -i "s/Designed for engineers/$siteName/" /srv/${siteVolName}/index.html 

# Lanzar contenedor


docker run --restart=always --name $siteVolName -d -p $puerto:80 -v /srv/${siteVolName}:/usr/share/nginx/html nginx

echo http://$(hostname -I | cut -d " " -f 1):$puerto 

```
